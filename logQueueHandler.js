var AWS = require('aws-sdk')

const ddb = new AWS.DynamoDB({ region: 'us-east-1', apiVersion: '2012-08-10' })

exports.handle = (event) => {
  if (!event || !event.Records || !event.Records.length === 0) {
    console.log('No Records found.')
    return
  }
  const now = new Date()
  var params = {
    TableName: 'Logs',
    Item: {
      appId: { S: 'sweet-app' },
      eventDateTime: { S: now.toISOString() },
      message: { S: event.Records[0]?.body },
    },
  }
  ddb.putItem(params, function (err, data) {
    if (err) {
      console.log('Error', err)
    } else {
      console.log('Success', data)
    }
  })
}
