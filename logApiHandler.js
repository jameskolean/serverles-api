// logHandler.js
const AWS = require('aws-sdk')

const sqs = new AWS.SQS({ region: 'us-east-1' })
const ddb = new AWS.DynamoDB({ region: 'us-east-1', apiVersion: '2012-08-10' })

exports.get = async (event) => {
  var params = {
    ExpressionAttributeValues: {
      ':appId': { S: 'sweet-app' },
      ':eventDateTime': { S: '2021-10-01T01:00:00.000Z' },
    },
    KeyConditionExpression: 'appId = :appId and eventDateTime > :eventDateTime',
    ProjectionExpression: 'appId, eventDateTime, message',
    TableName: 'Logs',
  }
  const data = await ddb.query(params).promise()

  console.log('data', data)
  return {
    statusCode: 200,
    body: JSON.stringify({
      message: 'Go Serverless v2.0! Your GET executed successfully!',
      log: data.Items,
    }),
  }
}

exports.post = async (event) => {
  await sqs
    .sendMessage({
      MessageAttributes: {
        Title: {
          DataType: 'String',
          StringValue: `Sample Serverless`,
        },
      },
      MessageBody: event.body,
      QueueUrl: process.env.SA_LOG_QUEUE_URL,
    })
    .promise()
  return {
    statusCode: 200,
    body: JSON.stringify({
      message: `Your data was added to Queue ${process.env.SA_LOG_QUEUE_URL}`,
      data: event.body,
    }),
  }
}
