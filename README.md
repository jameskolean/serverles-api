# Serverless Framework Example

This sample demonstrates how easily you can tie together AWS services using Serverless Framework. Serverless is an Open Source tool enabling you to define a serverless stack independent of a specific provider. In this example, we will focus on AWS.

This example supposes we want a way to save log messages in the cloud. We presume some Applications will POST a message to a REST API. The REST API uses a Lambda to place the message in a Queue. From the Queue, you can do whatever you like. In this case, de will write the message to DynamoDB. We will also add a REST API that reads the log messages.

## Get Started

[Install Serverless](https://www.serverless.com/framework/docs/getting-started) with npm.

```bash
npm install -g serverless
sls --verzion
```

Next set up your [AWS credentials](https://www.serverless.com/framework/docs/providers/aws/cli-reference/config-credentials).

```bash
serverless config credentials --provider provider --key key --secret secret
```

## Run it

Deploy the stack in AWS with `sls deploy`. That's it! You now have a complete stack.

Let's take it for a spin with Postman. Import `Serverless-API.postman_collection.json` into Postman and change the host variable to the one printed in the console you ran `sls deploy` in. Use POST to add Log messages and GET to pull the list of log messages.

Open AWS console and explore CloudFormation, SQS, Lambda, DynamoDB, and CloudWatch to get detailed information on the stack components.

Don't forget to clean up after yourself. You can remove the entire stack with `sls remove.`

## Inner Workings

The file `Serverless.yml` defines the entire stack. Here is a quick overview of the sections.

### provider

In this section, we create an IAM role. NOTE: in production, you want to be more specific when adding `Actions.` We also export an Environmental Variable for use in a Lambda.

### functions

In this section, we wire Lambda functions to the API Gateway and SQS. The actual Lambda functions are in `logApiHandler.js` and `logQueueHandler.js.`

### resources

In this section, we create a Queue with a Dead Letter Queue according to best practices. We also create a table in DynamoDB to store our log messages.
